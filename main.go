package main

import (
	"fmt"
	"net/http"
)

func main() {

	mux := http.NewServeMux()

	// diretorio fisico
	fs := http.FileServer(http.Dir("./static"))

	// mostra no browser localhost:8080
	mux.Handle("/", http.StripPrefix("/", fs))

	fmt.Println("Run Server 8080")
	http.ListenAndServe(":8080", mux)
}

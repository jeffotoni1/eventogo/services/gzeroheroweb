
const URL_GET_HERO = 'http://api.zerohero.eventogo.letsgophers.com:8080/v1/grpc/hero/';
const URL_GET_ALL_HERO = 'http://api.zerohero.eventogo.letsgophers.com:8080/v1/list/grpc/hero';

const fallbackResponse = {"response":"success","id":"332","uuid":"1af2150f-7951-4d2a-9eb0-fff69d2aaa41","name":"hulk","powerstats":{"intelligence":"88","strength":"100","speed":"63","durability":"100","power":"98","combat":"85"},"biography":{"full-name":"Bruce Banner","alter-egos":"No alter egos found.","aliases":["Annihilator","Captain Universe","Joe Fixit","Mr. Fixit","Mechano","Professor","Jade Jaws","Golly Green Giant"],"place-of-birth":"Dayton, Ohio","first-appearance":"Incredible Hulk #1 (May, 1962)","publisher":"Marvel Comics","alignment":"good"},"appearance":{"gender":"Male","race":"Human / Radiation","height":["8'0","244 cm"],"weight":["1400 lb","630 kg"],"eye-color":"Green","hair-color":"Green"},"work":{"occupation":"Nuclear physicist, Agent of S.H.I.E.L.D.","base":"(Banner) Hulkbuster Base, New Mexico, (Hulk) mobile, but prefers New Mexico"},"connections":{"group-affiliation":"Defenders, former leader of the new Hulkbusters, member of the Avengers, Pantheon, Titans Three, the Order, Hulkbusters of Counter-Earth-Franklin, alternate Fantastic Four","relatives":"Betty Ross Talbot Banner (wife), Brian Banner (father, apparently deceased), Rebecca Banner (mother, deceased), Morris Walters (uncle), Elaine Banner Walters (aunt, deceased), Jennifer Walters (She-Hulk, cousin), Thaddeus E. 'Thunderbolt' Ross (father"},"image":{"url":"https://www.superherodb.com/pictures2/portraits/10/100/83.jpg"}};

$(document).ready(() => {
	getHeroes();

	$('.btn-get-hero').trigger('click');

	// Configuração das mensagens de alerta no topo
	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-top-full-width",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "2500",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};

	$('#basic-addon3, #basic-addon3-battle').html(URL_GET_HERO);
});

$(document).on('click', '.btn-get-hero', () => {
	const elButton = $('.btn-get-hero');
	let searchHero = $('#search-hero').val();

	if (searchHero) {

		const searchHeroParts = searchHero.split('/');

		let url = URL_GET_HERO + searchHero;

		if (searchHeroParts.length == 1) {

			showLoaderButton(elButton, () => {

				const timeStart = new Date().getTime();
				let timeEnd = null;
				let hasErrors = false;
				$.get(url, (data, status) => {
					timeEnd = new Date().getTime() - timeStart;

					setHero(data);

					$('#dominioInfo, #dominioInfoBattle').hide();
					$('#timeCallInfo').show();
				})
				.fail((error) => {
					timeEnd = new Date().getTime() - timeStart;

					setHero(fallbackResponse);

					$('#dominioInfo, #dominioInfoBattle').html(`
						<span class="text-dominio">
							<i class="fa fa-warning"></i> &nbsp; Dom&iacute;nio encontra-se indispon&iacute;vel
						</span>
					`);

					$('#timeCallInfo').hide();
					$('#dominioInfo, #dominioInfoBattle').show();

					hasErrors = true;
				})
				.always((respAlways) => {
					$('#timeCall').html(timeEnd + 'ms');

					$('.bg-hero-battle').hide();
					$('.bg-hero-stats').show();
					$('.row-description').show();

					hideLoaderButton(elButton);

					if (hasErrors) {
						setTimeout(() => {
							$('#search-hero').attr('disabled', 'disabled');
							$('#search-hero').prop('disabled', true);
							
							$('#button-addon2').attr('disabled', 'disabled');
							$('#button-addon2').prop('disabled', true);

							$('#search-hero-battle').attr('disabled', 'disabled');
							$('#search-hero-battle').prop('disabled', true);
							
							$('#button-addon2-battle').attr('disabled', 'disabled');
							$('#button-addon2-battle').prop('disabled', true);
						}, 300);
					}
				});

			});

		}
	}
});

$(document).on('click', '.btn-get-hero-battle', () => {
	const elButton = $('.btn-get-hero-battle');
	let searchHero = $('#search-hero-battle').val();

	if (searchHero) {

		const searchHeroParts = searchHero.split(',');

		let url = URL_GET_HERO + searchHero;

		showLoaderButton(elButton, () => {

			const timeStart = new Date().getTime();
			let timeEnd = null;
			let hasErrors = false;
			$.get(url, (data, status) => {
				timeEnd = new Date().getTime() - timeStart;

				$('#dominioInfo, #dominioInfoBattle').hide();
				$('#timeCallInfoBattle').show();
			})
			.fail((error) => {
				timeEnd = new Date().getTime() - timeStart;

				hasErrors = true;

				$('#dominioInfo, #dominioInfoBattle').html(`
					<span class="text-dominio">
						<i class="fa fa-warning"></i> &nbsp; Dom&iacute;nio encontra-se indispon&iacute;vel
					</span>
				`);

				$('#timeCallInfoBattle').hide();
				$('#dominioInfo, #dominioInfoBattle').show();
			})
			.always(() => {
				$('#timeCallBattle').html(timeEnd + 'ms');
				$('#timeCallBattleInfo').show();

				hideLoaderButton(elButton);

				$('.bg-hero-stats').hide();
				$('.row-description').hide();
				$('.bg-hero-battle').show();

				if (hasErrors) {
					setTimeout(() => {
						$('#search-hero-battle').attr('disabled', 'disabled');
						$('#search-hero-battle').prop('disabled', true);
						
						$('#button-addon2-battle').attr('disabled', 'disabled');
						$('#button-addon2-battle').prop('disabled', true);

						$('#search-hero').attr('disabled', 'disabled');
						$('#search-hero').prop('disabled', true);
						
						$('#button-addon2').attr('disabled', 'disabled');
						$('#button-addon2').prop('disabled', true);
					}, 300);
				}
			});

		});
	}
});

$(document).on('click', '.btn-link-suggestion', (event) => {
	if (event.target !== null && event.target !== undefined) {

		const hero = $(event.target).attr('data-hero');

		if (hero) {
			$('#search-hero').val(hero);
		}
	}
});

const POWER_STATS = [
	'intelligence',
	'strength',
	'speed',
	'durability',
	'power',
	'combat',
];

let HEROES = [
	'hulk',
	'ironman',
	'superman',
	'wonderwoman',
	'legion',
	'wolverine',
	'lukeskywalker',
	'batman',
];

const HEROES_TMP = [
	'hulk',
	'iron-man',
	'super-man',
	'wonder-woman',
	'legion',
	'wolverine',
	'luke-skywalker',
	'batman',
]

$(document).on('click', '.btn-link-battle-suggestion', (event) => {

	const maxPowerStats = POWER_STATS.length - 1;
	const randomPowerStat = getRandomInt(maxPowerStats);
	let powerstat = POWER_STATS[randomPowerStat] !== null && POWER_STATS[randomPowerStat] !== undefined ? POWER_STATS[randomPowerStat] : 'speed';

	$('#optPowerStats').val(powerstat);
	$('#optPowerStats').trigger('change');

	let heroesAux = HEROES.map((x) => x);
	let heroes = [];
	let hero;

	let maxHeroes;
	let randomHero;


	for (var i = 0; i < 3; i++) {

		maxHeroes = heroesAux.length - 1;
		randomHero = getRandomInt(maxHeroes);
		
		hero = heroesAux[randomHero] !== null && heroesAux[randomHero] !== undefined ? heroesAux[randomHero] : null;
		if (hero) {
			heroes.push(hero);
			heroesAux.splice(randomHero, 1);
		}
	}

	if (heroes.length > 0) {
		$('#search-hero-battle').val(heroes.join(','));
	}
});

const getRandomInt = (max) => {
  return Math.floor(Math.random() * max);
}


const showLoaderButton = (elButton, callback) => {
    $(elButton).attr('disabled', 'disabled');
	$(elButton).prop('disabled', true);

    $(elButton).blur().find('.text').fadeOut(100, function () {
        $(elButton).find('.loader').fadeIn(100, function () {
            setTimeout(function () {
                if (callback) {
                	callback();
                }
            }, 1000);
        });
    });
};

const hideLoaderButton = (elButton) => {
    $(elButton).find(".loader").fadeOut(100, function () {
    	$(elButton).removeAttr("disabled").find(".text").fadeIn(100);
    	$(elButton).prop('disabled', false);
    });
};

const setHero = (data) => {
	$('.get-hero-json-container').show();
	$('#get-hero-json').html(JSON.stringify(data, null, 4));

	if (data.name !== null && data.name !== undefined) {
		$('#hero-name').html(data.name.toUpperCase());
	}

	if (data.image !== null && data.image !== undefined) {
		if (data.image.url !== null && data.image.url !== undefined) {
			$('#hero-img').attr('src', data.image.url).show();
		}
	}

	if (data.biography !== null && data.biography !== undefined) {
		if (data.biography['full-name'] !== null && data.biography['full-name'] !== undefined) {
			$('#hero-description').html(data.biography['full-name']);
			$('#biography_full-name').html(data.biography['full-name']);
		}

		if (data.biography['alter-egos'] !== null && data.biography['alter-egos'] !== undefined) {
			$('#biography_alter-egos').html(data.biography['alter-egos']);
		}

		if (data.biography['aliases'] !== null && data.biography['aliases'] !== undefined && data.biography['aliases'].length > 0) {
			$('#biography_aliases').html(data.biography['aliases'].join(', '));
		}

		if (data.biography['place-of-birth'] !== null && data.biography['place-of-birth'] !== undefined) {
			$('#biography_place-of-birth').html(data.biography['place-of-birth']);
		}

		if (data.biography['first-appearance'] !== null && data.biography['first-appearance'] !== undefined) {
			$('#biography_first-appearance').html(data.biography['first-appearance']);
		}

		if (data.biography['publisher'] !== null && data.biography['publisher'] !== undefined) {
			$('#biography_publisher').html(data.biography['publisher']);
			$('.biography_publisher').html('Publicado por: ' + data.biography['publisher']);
		}

		if (data.biography['alignment'] !== null && data.biography['alignment'] !== undefined) {
			$('#biography_alignment').html(data.biography['alignment']);
		}
	}

	if (data.appearance !== null && data.appearance !== undefined) {
		if (data.appearance['gender'] !== null && data.appearance['gender'] !== undefined) {
			$('#appearance_gender').html(data.appearance['gender']);
		}

		if (data.appearance['race'] !== null && data.appearance['race'] !== undefined) {
			$('#appearance_race').html(data.appearance['race']);
		}

		if (data.appearance['height'] !== null && data.appearance['height'] !== undefined && data.appearance['height'].length > 0) {
			$('#appearance_height').html(data.appearance['height'].join(' / '));
		}

		if (data.appearance['weight'] !== null && data.appearance['weight'] !== undefined && data.appearance['weight'].length > 0) {
			$('#appearance_weight').html(data.appearance['weight'].join(' / '));
		}

		if (data.appearance['eye-color'] !== null && data.appearance['eye-color'] !== undefined) {
			$('#appearance_eye-color').html(data.appearance['eye-color']);
		}

		if (data.appearance['hair-color'] !== null && data.appearance['hair-color'] !== undefined) {
			$('#appearance_hair-color').html(data.appearance['hair-color']);
		}
	}

	if (data.work !== null && data.work !== undefined) {
		if (data.work['occupation'] !== null && data.work['occupation'] !== undefined) {
			$('#work_occupation').html(data.work['occupation']);
		}

		if (data.work['base'] !== null && data.work['base'] !== undefined) {
			$('#work_base').html(data.work['base']);
		}
	}

	if (data.connections !== null && data.connections !== undefined) {
		if (data.connections['group-affiliation'] !== null && data.connections['group-affiliation'] !== undefined) {
			$('#connections_group-affiliation').html(data.connections['group-affiliation']);
		}

		if (data.connections['relatives'] !== null && data.connections['relatives'] !== undefined) {
			$('#connections_relatives').html(data.connections['relatives']);
		}
	}

	if (data.powerstats !== null && data.powerstats !== undefined) {

		if (data.powerstats.intelligence !== null && data.powerstats.intelligence !== undefined) {
			$('#pb-intelligence').html(data.powerstats.intelligence);
			$('#pb-intelligence').css({ width: data.powerstats.intelligence + '%' });
		}

		if (data.powerstats.strength !== null && data.powerstats.strength !== undefined) {
			$('#pb-strength').html(data.powerstats.strength);
			$('#pb-strength').css({ width: data.powerstats.strength + '%' });
		}

		if (data.powerstats.speed !== null && data.powerstats.speed !== undefined) {
			$('#pb-speed').html(data.powerstats.speed);
			$('#pb-speed').css({ width: data.powerstats.speed + '%' });
		}

		if (data.powerstats.durability !== null && data.powerstats.durability !== undefined) {
			$('#pb-durability').html(data.powerstats.durability);
			$('#pb-durability').css({ width: data.powerstats.durability + '%' });
		}

		if (data.powerstats.power !== null && data.powerstats.power !== undefined) {
			$('#pb-power').html(data.powerstats.power);
			$('#pb-power').css({ width: data.powerstats.power + '%' });
		}

		if (data.powerstats.combat !== null && data.powerstats.combat !== undefined) {
			$('#pb-combat').html(data.powerstats.combat);
			$('#pb-combat').css({ width: data.powerstats.combat + '%' });
		}
	}

	// chamada do Prism para colocar o estilo de código no texto
	Prism.highlightElement($('#get-hero-json')[0]);
};


const getHeroes = function () {
	let url = URL_GET_ALL_HERO;

	$.get(url, (data, status) => {

		if (data !== null && data.length > 0) {
			loadOptionsHeroes(data);
		}

	})
	.fail((error) => {

		loadOptionsHeroes(HEROES_TMP);
	})
	.always(() => {
		HEROES = HEROES_TMP.map((hero) => {
			return hero.replaceAll('', '');
		});
	});
};


const loadOptionsHeroes = (dataHeroes) => {
	let htmlOptions = '';

	dataHeroes.forEach(function (hero, indexHero) {
		heroName = hero.replaceAll('', '');
		htmlOptions += `<option value="${heroName}">${heroName}</option>`;
	});
	
	$('#optHeroes').html(htmlOptions);
}
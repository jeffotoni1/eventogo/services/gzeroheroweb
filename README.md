# gzeroheroweb

```bash
$ docker build -f Dockerfile --no-cache --build-arg NETRCUSER=$USER2 --build-arg NETRCPASS=$TOKEN2 -t jeffotoni/gzeroheroweb .
```

```bash
$ docker run --name gzeroheroweb --rm -p 8080:8080 jeffotoni/gzeroheroweb
```